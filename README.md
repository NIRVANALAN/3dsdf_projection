# E3DGE CVPR 2023

## inference Scripts

### 2D Metrics

```
bash scripts/test/eval_2dmetrics_ffhq.sh
```

### Editing:
```
bash scripts/test/editing/trajectory_output/v9.sh # trajectory, 8 frames azim
```

Better editing performance with 2D alignment only:
```
bash scripts/test/editing/trajectory_output/v9.sh # trajectory, 8 frames azim
```


Inversion:

with Decoder:
- Code: scripts/test/inference_alignment_withDecoder.sh

- [ ] batch inference (eval_batch=4?)

### Evaluation

Input view:


```
# trajectory:
# optimization-based method
bash scripts/test/eval_trajectory/eval_2dmetrics_trajectory_optimization_based.sh

# encoder-based method
bash scripts/test/eval_trajectory/eval_2dmetrics_trajectory_optimization.sh
```

### Ablations
final_organized_logs/checkpoints/ablations/v2_pure3D_depthPE.pt # + 3D align
final_organized_logs/checkpoints/v7_advweight001.pt # 2D align
final_organized_logs/checkpoints/V9/occlusion_runner_cycle_v9_hybridAlign_advLoss0.01_finetuneFromV7_models_latest.pt # final hybrid version


### Editing

```

```

# training

## stage 1

train 3D: 
```
scripts/Final_Organized_Scripts/train/stage1.sh # validate
```

train 2D + decoder, globalwise:
```
scripts/Final_Organized_Scripts/train/stage1.5.sh # validate
```

do full evaluation:


## stage 2

train the 3D reconstruction pipeline (curriculum):

train the ADA reconstruction pipeline (curriculum):

train the fusion pipeline:


# Evalaution

output (OKR of stage 1): 

- [ ] 1. low-res rec
- [ ] 2. high-res rec
- [ ] 3. geometry mesh
- [ ] 4. geometry metrics, synthetic.
- [ ] 5. trajectory video (low or high)
- [ ] 6. trajectory metrics.
- [ ] 7. 2D metrics.

--------



# Data Structure

1. all the Tensor shape should be B H W Steps _ in this repo.o


# Data

shapenet:

https://drive.google.com/drive/folders/1OkYgeRcIcLOFu1ft5mRODWNQaPJ0ps90

## Inversion
Folder: ```scripts/Final_Organized_Scripts/projections```

```
# w-space
# wplus-space
# PTI
```

### Reproduce inversion baselines

Editing: 
PTI:
```
bash scripts/test/editing/trajectory_output/PTI.sh
```

W space:
```
bash scripts/Final_Organized_Scripts/projections/w.sh
```


Misc
1. helper_scripts, gallary_video and other helper scripts. explain. how to use calc_losses_on_images.py?



<div align="center">

<h1>
E3DGE: Self-Supervised Geometry-Aware Encoder for Style-based 3D GAN Inversion
</h1>

<div>
    <a href='https://github.com/NIRVANALAN' target='_blank'>Yushi Lan</a><sup>1</sup>&emsp;
    <a href='' target='_blank'>Xuyi Meng</a><sup>1</sup>&emsp;
    <a href='https://williamyang1991.github.io/' target='_blank'>Shuai Yang</a><sup>1</sup>&emsp;
    <a href='https://www.mmlab-ntu.com/person/ccloy/' target='_blank'>Chen Change Loy</a>
    <sup>1</sup> &emsp;
    <a href='https://daibo.info/' target='_blank'>Bo Dai</a>
    <sup>2</sup>
</div>
<div>
    S-Lab, Nanyang Technological University<sup>1</sup>;
    &emsp;
    Shanghai Artificial Intelligence Laboratory <sup>2</sup>
    <!-- ; <sup>*</sup>corresponding author -->
</div>

<br>

<h4>
E3DGE is an encoder-based 3D GAN inversion framework that yields high-quality shape and texture reconstruction.
</h4>

<table>
<tr>
    <!-- <td><img src="assets/322.jpg" width="100%"/></td> -->
     <td rowspan="2"><img src="assets/322.jpg" width="100%"/></td>
    <td><img src="assets/inversion/322_tex.gif" width="100%"/></td>
    <td><img src="assets/editing/tex_0_opt.gif" width="100%"/></td>
    <td><img src="assets/editing/tex_1.0_opt.gif" width="100%"/></td>
    <td><img src="assets/toonify/322_tex.gif" width="100%"/></td>
</tr>

<tr>
    <!-- <td><img src="assets/322.jpg" width="100%"/></td> -->
    <!-- <td></td> -->
    <td><img src="assets/inversion/322_geo.gif" width="100%"/></td>
    <td><img src="assets/editing/geo_0_opt.gif" width="100%"/></td>
    <td><img src="assets/editing/geo_1.0_opt.gif" width="100%"/></td>
    <td><img src="assets/toonify/322_geo.gif" width="100%"/></td>
</tr>

<tr>
    <td align='center' width='20%'>Input</td>
    <td align='center' width='20%'>Inversion</td>
    <td align='center' width='20%'>Editing <br> (-Smile)</td>
    <td align='center' width='20%'>Editing <br> (+Smile)</td>
    <td align='center' width='20%'>Toonify</td>
</tr>

</table>

For more visual results, go checkout our <a href="https://nirvanalan.github.io/projects/E3DGE/index.html" target="_blank">project page</a> :page_with_curl:

Codes coming soon :facepunch:
<!-- This repository will contain the official implementation of _E3DGE: 
Self-supervised Geometry-Aware Encoder for Style-based 3d GAN Inversion_. -->

---

<h4 align="center">
  <a href="https://nirvanalan.github.io/projects/E3DGE/index.html" target='_blank'>[Project Page]</a> •
  <a href="https://arxiv.org/abs/2212.07409" target='_blank'>[arXiv]</a> •
  <a href="https://drive.google.com/file/d/1yDkJfJOLeVlON7ZdRSnR34Ra_ikTVI0A/preview" target='_blank'>[Demo Video]</a>
</h4>
</div>

## :mega: Updates

[06/2023] Inference and training codes on FFHQ with StyleSDF base model are released, including colab demo.

[03/2023] E3DGE is accepted to CVPR 2023 :partying_face:! Code coming soon.


## :dromedary_camel: TODO

- [x] Release the inference and training code.
- [ ] Release Hugging face demo.
- [ ] Release SG2/PTI optimization-based inversion code using StyleSDF.
- [ ] Release pre-traind models using EG3D as the base model.
- [ ] Release video inversion code.

## :handshake: Citation
If you find our work useful for your research, please consider citing the paper:
```
@inproceedings{lan2022e3dge,
  title={E3DGE: Self-Supervised Geometry-Aware Encoder for Style-based 3D GAN Inversion},
  author={Lan, Yushi and Meng, Xuyi and Yang, Shuai and Loy, Chen Change and Dai, Bo},
  booktitle={Computer Vision and Pattern Recognition (CVPR)},
  year={2023}
}
```

## :desktop_computer: Requirements

NVIDIA GPUs are required for this project.
We have test the inference codes on NVIDIA RTX2080Ti and NVIDIA V100.
The training codes have been tested on NVIDIA V100. 
We recommend using anaconda to manage the python environments.

```bash
conda create --name e3dge python=3.8
conda install -c conda-forge ffmpeg
conda install pytorch==1.9.0 torchvision==0.10.0 torchaudio==0.9.0 cudatoolkit=10.2 -c pytorch
conda install -c fvcore -c iopath -c conda-forge fvcore iopath
pip install -r requirements.txt
```

## :running_woman: Inference

### Download Models

The pretrain 3D generators and encoders are needed for inference.

The following script downloads pretrain models.

```bash
python download_models.py
```

Reproduce the results in Table 1 (Quantitative performance on CelebA-HQ.)

```
bash scripts/Final_Organized_Scripts/test/eval_2dmetrics_ffhq.sh
```

do novel view synthesis given 2D images (on some demo images):
```
bash scripts/Final_Organized_Scripts/test/eval_view_synthesis.sh
```

Conduct semantics editing (on some demo images):
```
bash scripts/Final_Organized_Scripts/test/editing.sh
```

3D Toonifications with our pre-triaind encoder:
```
bash scripts/Final_Organized_Scripts/test/toonify.sh
```

## :train: Training

For all the experiments, we use `4 V100 GPUs` by default.

### FFHQ

#### Download Pre-trained generators and pre-processed test set

TODO: 
```bash
python download_models.py # download pre-trained models
python download_datasets.py # download preprocessed CelebA-HQ test set
```

#### Commands

stage 1 training (Sec. 1, Self-supervision for plausible shape inversion.)
```
bash scripts/Final_Organized_Scripts/train/ffhq/stage1.sh
```

stage 2.1 training (Sec. 2+3, Local feature fusion for high-fidelity inversion. Using 2D alignment only)

```
bash scripts/Final_Organized_Scripts/train/ffhq/stage2.1.sh
```

stage 2.2 training (Sec. 3, Hybrid feature alignment for high-quality editing)

```
# update the ffhq/afhq dataset path in the bash if adv_lambda != 0 (enables adversarial training)
bash scripts/Final_Organized_Scripts/train/ffhq/stage2.2.sh
```

Intermediate results will be saved under `???` every `2000` iterations. The first line presents inference images from EMA generator. The second line present one inference sample of the training generator and one sample from the training dataset.

To inference the trained models, please refer to the **Inference** section.

Support for more datasets coming soon...

## :raised_hands: Acknowledgements

This study is supported under the RIE2020 Industry Alignment Fund Industry Collaboration Projects (IAF-ICP) Funding Initiative, as well as cash and in-kind contribution from the industry partner(s). It is also partially supported by Singapore MOE AcRF Tier 2 (MOE-T2EP20221-0011) and the NTU URECA research program.


## :newspaper_roll:  License

Distributed under the S-Lab License. See `LICENSE` for more information.